var accordions = document.getElementsByClassName("accordion");
var length = accordions.length;

for (let i = 0; i < length; i++) {
    accordions[i].addEventListener("click", function (event) {

        let numberOfHeader = event.target.id.match(/\d/g);
        let contents = document.getElementsByClassName("to-hide");
        let contentsLength = contents.length;

        for (let j = 0; j < contentsLength; j++) {
            if ((parseInt(contents[j].id.match(/\d/g)) == parseInt(numberOfHeader))
                && !(contents[j].classList.contains('content'))) {
                contents[j].style.maxHeight = contents[j].scrollHeight + "px";
                contents[j].classList.add("content");
            } else {

                contents[j].style.maxHeight = null;
                contents[j].classList.remove("content");
            }
        }
    });
}