var accordions = document.getElementsByClassName("skill-bar");
var length = accordions.length;

for (let i = 0; i < length; i++) {
    accordions[i].addEventListener("mouseout", function (event) {
        let numberOfSkill = this.id.match(/\d/g);
        let skillText = document.getElementById("skill_text_" + numberOfSkill);


        skillText.style.maxHeight = null;

    });
}